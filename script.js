const resizable = document.querySelectorAll('.resizable');
const sidebar = document.getElementById('sidebar');
const main = document.getElementById('main');
const divider = document.getElementById('divider');
let isResizing = false;
let lastDownX;
let initialSidebarWidth;

resizable.forEach((el) => {
    el.addEventListener('mousedown', (e) => {
        isResizing = true;
        lastDownX = e.clientX;
        initialSidebarWidth = sidebar.getBoundingClientRect().width;
    });
});

document.addEventListener('mouseup', () => {
    isResizing = false;
});

document.addEventListener('mousemove', (e) => {
    if (!isResizing) return;

    const diff = lastDownX - e.clientX;
    const newWidth = initialSidebarWidth - diff;

    // Ensure the newWidth is within reasonable limits
    const minWidth = 150;
    const maxWidth = resizable[0].getBoundingClientRect().width - 150;

    if (newWidth > minWidth && newWidth < maxWidth) {
        sidebar.style.flexBasis = newWidth + 'px';
        main.style.flexBasis = `calc(100% - ${newWidth}px)`;
    }
});
